import React, {Component, Fragment} from 'react';
import Toolbar from './components/UI/Toolbar/Toolbar';
import UrlForm from "./components/URLForm/URLForm";

class App extends Component {
  render() {
    return (
      <Fragment>
        <header><Toolbar/></header>
        <main className="container">
<UrlForm/>
        </main>
      </Fragment>
    );
  }
}

export default App;
