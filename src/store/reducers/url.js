import {FETCH_URL_SUCCESS} from "../actions/actionTypes";

const initialState = {
  url: ''
};

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case FETCH_URL_SUCCESS:
      return {...state, url: action.url};
    default:
      return state;
  }
};

export default reducer;