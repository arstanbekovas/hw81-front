import axios from '../../axios-api';
import {CREATE_URL_SUCCESS, FETCH_URL_SUCCESS} from "./actionTypes";

export const fetchUrlSuccess = url => {
  return {type: FETCH_URL_SUCCESS, url};
};

export const fetchUrl = () => {
  return dispatch => {
    axios.get('/links').then(
      response => dispatch(fetchUrlSuccess(response.data))
    );
  }
};

export const createUrlSuccess = () => {
  return {type: CREATE_URL_SUCCESS};
};

export const shortenUrl = UrlData => {
  return dispatch => {
    return axios.post('/links', UrlData).then(
      response => dispatch(fetchUrlSuccess(response.data.shortUrl))
    );
  };
};