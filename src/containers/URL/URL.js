import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import { Panel} from "react-bootstrap";
import {fetchUrl} from "../../store/actions/url";
import {Link} from "react-router-dom";

class URL extends Component {
  componentDidMount() {
    this.props.onFetchUrl();
  }

  render() {
    return (
      <Fragment>
        <h2>Your link now look like this</h2>
        <Panel.Body>
          <Link >
            {this.props.url}
          </Link>
        </Panel.Body>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    url: state.url.url
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchUrl: () => dispatch(fetchUrl())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(URL);