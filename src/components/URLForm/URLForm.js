import React, {Component} from 'react';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";
import {shortenUrl} from "../../store/actions/url";
import {connect} from "react-redux";

class UrlForm extends Component {
  state = {
    URL: ''
  };

  submitFormHandler = event => {
    event.preventDefault();

    this.props.shortenUrl({originalUrl: this.state.URL});
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  render() {
    return (
      <Form horizontal onSubmit={this.submitFormHandler}>
        <FormGroup controlId="URL">
          <Col componentClass={ControlLabel} sm={2}>
            Shorten your URL
          </Col>
          <Col sm={10}>
            <FormControl
              type="text" required
              placeholder="Enter URL"
              name="URL"
              value={this.state.URL}
              onChange={this.inputChangeHandler}
            />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" type="submit">Shorten!</Button>
          </Col>
        </FormGroup>
        <FormGroup>
          <h2>Your link now look like this</h2>
          <a href={'http://localhost:8000/links/' + this.props.url} target="_blank">{this.props.url ? 'http://localhost:8000/links/' + this.props.url : null}</a>
        </FormGroup>
      </Form>
    );
  }
}

const mapStateToProps = state => {
  return {
    url: state.url.url
  }
};

const mapDispatchToProps = dispatch => {
  return {
    shortenUrl: (data) => dispatch(shortenUrl(data))
  }
};

export default connect(mapStateToProps, mapDispatchToProps) (UrlForm);
